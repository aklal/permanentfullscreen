package eu.aklal.permanent.fullscreen.screens.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import eu.aklal.permanent.fullscreen.data.SampleItem
import eu.aklal.permanent.fullscreen.databinding.AppItemBinding

class SampleAdapter (
    private val dataset: List<SampleItem>,
) : RecyclerView.Adapter<SampleAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = AppItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = item.text
    }

    override fun getItemCount(): Int = dataset.size

    class ItemViewHolder(val binding: AppItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val textView: TextView = binding.textViewApp
    }
}