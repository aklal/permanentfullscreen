package eu.aklal.permanent.fullscreen.screens.intro

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.Util
import androidx.media3.exoplayer.ExoPlayer
import androidx.navigation.fragment.findNavController
import eu.aklal.permanent.fullscreen.databinding.FragmentIntroBinding
import eu.aklal.permanent.fullscreen.utils.OnSwipeTouchListener

/**
 * Display a video in fullscreen. The video plays in loop until the user swipe the screen up.
 * The path to the video to display is hardcoded.
 */
@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
class IntroFragment: Fragment()  {

    private var _binding: FragmentIntroBinding? = null
    private val binding get() = _binding!!

    private var player: ExoPlayer? = null
    private var playWhenReady = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentIntroBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // After a swipe up gesture the Home Fragment is displayed
        view.setOnTouchListener(object : OnSwipeTouchListener(context) {
            override fun onSwipeUp(): Boolean {
                val action = IntroFragmentDirections.actionIntroFragmentToHomeFragment()
                findNavController().navigate(action)
                return true
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Initialise the video to play in loop
     */
    private fun initializePlayer() {
        player = context?.let {
            ExoPlayer.Builder(it)
                .build()
                .also { exoPlayer ->
                    exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
                    exoPlayer.playWhenReady = playWhenReady
                    exoPlayer.prepare()

                    binding.videoView.player = exoPlayer
                    val mediaItem = MediaItem.fromUri("file:///android_asset/vid/intro/banana.mp4")
                    exoPlayer.setMediaItem(mediaItem)
                }
        }
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        player?.let { exoPlayer ->
            exoPlayer.release()
        }
        player = null
    }
}