package eu.aklal.permanent.fullscreen.screens

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.findNavController
import eu.aklal.permanent.fullscreen.R
import eu.aklal.permanent.fullscreen.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        //hideBars()

        setContentView(view)
        hideSystemUI(view)
    }

    override fun onResume() {
        super.onResume()
        //hideStatusBar() // NO EFFECT
        val navController = findNavController(R.id.nav_host_fragment)
        navController.navigate(R.id.introFragment)
        hideStatusBars()
    }

    private fun hideStatusBar(){
        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
        actionBar?.hide()
    }

    fun hideBars(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private fun hideSystemUI(view: View) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, view).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }

    }
    fun hideStatusBars() {
        val view = window.decorView
        val uiOption = View.SYSTEM_UI_FLAG_FULLSCREEN
        view.systemUiVisibility = uiOption
        val actionBar = actionBar
        actionBar?.hide()
    }

    companion object {
        private const val REQUEST_CODE = 10101
    }
}
