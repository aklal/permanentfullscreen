package eu.aklal.permanent.fullscreen

import android.app.Application
import eu.aklal.permanent.fullscreen.utils.CrashReportingTree
import eu.aklal.permanent.fullscreen.utils.HyperlinkedDebugTree
import timber.log.Timber

class MainApplication : Application(){
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(HyperlinkedDebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }
}