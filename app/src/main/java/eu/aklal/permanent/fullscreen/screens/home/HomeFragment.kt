package eu.aklal.permanent.fullscreen.screens.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import eu.aklal.permanent.fullscreen.R
import eu.aklal.permanent.fullscreen.databinding.FragmentHomeBinding
import eu.aklal.permanent.fullscreen.utils.getItemsFromJson
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import java.util.*

class HomeFragment : Fragment() {
    var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    val job = Job()
    val coroutineContext = Dispatchers.IO + job
    val delayBeforeNavToIntro: Long = 5000

    private fun setTimer(){
        lifecycleScope.launch(coroutineContext){
            delay(delayBeforeNavToIntro)

            val currentDest = findNavController().currentDestination == findNavController().findDestination(
                R.id.homeFragment)
            if (currentDest) {
                withContext(Main) {
                    val action = HomeFragmentDirections.actionHomeFragmentToIntroFragment()
                    findNavController().navigate(action)
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context = requireContext()
        // Initialize data - should be done in ModelView
        val myDataset = context.getItemsFromJson()

        binding.recyclerView.adapter = SampleAdapter(myDataset)

        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        binding.recyclerView.setHasFixedSize(true)
    }
}