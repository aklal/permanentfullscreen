package eu.aklal.permanent.fullscreen.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SampleItem (
    @SerialName("text")
    val text: String
)