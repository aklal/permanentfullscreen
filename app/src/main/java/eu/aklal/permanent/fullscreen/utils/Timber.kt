package eu.aklal.permanent.fullscreen.utils

import android.util.Log
import timber.log.Timber


/**
 * A [Tree] for debug builds.
 * Automatically shows a Hyperlink to the calling Class and Linenumber in the Logs.
 * Allows quick lookup of the caller source just by clicking on the Hyperlink in the Log.
 * @see https://github.com/JakeWharton/timber/pull/377/files
 */
class HyperlinkedDebugTree(private val showMethodName: Boolean = true) : Timber.DebugTree() {
    override fun createStackElementTag(element: StackTraceElement) =
        with(element) {
            "($fileName:$lineNumber) ${if (showMethodName) " $methodName()" else ""}"
        }
}

/**
 * Log message with information of the thread used
 */
fun infoWorkingIn(msg: String) {
    val thread = "[🧵 ${Thread.currentThread().name}]"
    Timber.i("${msg.padEnd(54)} $thread")
}


/**
 * A [Tree] which logs important information for crash reporting on release builds.
 * [FakeCrashLibrary] is used as placeholder for a real framework to call.
 * */
class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
        FakeCrashLibrary.log(priority, tag, message)
        if (t != null) {
            if (priority == Log.ERROR) {
                FakeCrashLibrary.logError(t)
            } else if (priority == Log.WARN) {
                FakeCrashLibrary.logWarning(t)
            }
        }
    }
}