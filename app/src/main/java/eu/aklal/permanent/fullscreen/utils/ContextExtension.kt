package eu.aklal.permanent.fullscreen.utils

import android.content.Context
import eu.aklal.permanent.fullscreen.data.SampleItem
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import timber.log.Timber

/**
 * A context extension to provide a list of object based on
 * the content of a json file.
 * @return List<SampleItem> a list of objects
 */
fun Context.getItemsFromJson(): List<SampleItem>{
    val jsonString: String = assets.open("json/samples.json")
        .bufferedReader()
        .use { it.readText() }

    Timber.i("~ list of samples: ${Json.decodeFromString<List<SampleItem>>(jsonString)}")
    return Json.decodeFromString<List<SampleItem>>(jsonString)
}
